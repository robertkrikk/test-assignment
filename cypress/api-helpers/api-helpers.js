export const SESSIONS_URL = 'https://magic.saas-3.veriff.me/api/v2/sessions'
export const CREATE_SESSION_URL = 'https://demo.saas-3.veriff.me/'

/**
 * Create a request, which has body sent with the payload
 * @param method - HTTP method
 * @param url - Endpoint, where the request is sent
 * @param headers - Headers, which are sent with the request
 * @param body - Request body
 * @returns {Cypress.Chainable<Cypress.Response>}
 */
export const createRequestWithBody = (method, url, headers, body) => {
  return cy.request({
    method,
    url,
    headers,
    body,
    failOnStatusCode: false
  })
}
