import DeviceSelectionPage from './device-selection-page'

const FULL_NAME_FIELD = 'input[name="name"]'
const SESSION_LANGUAGE_FIELD = 'button[name="language"]'
const LANGUAGE_OPTIONS = '#downshift-0-menu'
const DOCUMENT_COUNTRY_FIELD = 'input[name="documentCountry"]'
const DOCUMENT_TYPE_FIELD = 'button[name="documentType"]'
const VERIFF_ME_BUTTON = 'button[type="submit"]'
const ESTONIAN_DOCUMENT_COUNTRY_OPTION = '[data-testid="EE"]'

class HomePage {
  static visit () {
    cy.visit('/')
  }

  static enterFullName (name) {
    cy.get(FULL_NAME_FIELD)
      .clear()
      .type(name)
  }

  static selectLanguage (language) {
    cy.get(SESSION_LANGUAGE_FIELD)
      .click()
    cy.get(LANGUAGE_OPTIONS)
      .contains(language)
      .click({ force: true })
  }

  static selectCountry (country) {
    cy.get(DOCUMENT_COUNTRY_FIELD)
      .type(country)
    cy.get(ESTONIAN_DOCUMENT_COUNTRY_OPTION)
      .click()
  }

  static selectDocumentType (documentType) {
    cy.get(DOCUMENT_TYPE_FIELD)
      .click()
    cy.contains(documentType)
      .click({ force: true })
  }

  static pressVeriffMe () {
    cy.get(VERIFF_ME_BUTTON)
      .click()
    return new DeviceSelectionPage()
  }
}

export default HomePage
