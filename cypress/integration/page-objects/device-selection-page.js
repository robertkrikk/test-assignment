import StartSessionPage from './start-session-page'

const CURRENT_DEVICE_FIELD_TEXT = 'Continue with your current device'
const PHONE_NUMBER_SUBMIT = '[data-testid="phone-number-submit-button"]'

class DeviceSelectionPage {
  static shouldBeVisible () {
    // Enter the iframe and check if user is in the new modal
    cy.enter().then(getBody => {
      getBody()
        .find(PHONE_NUMBER_SUBMIT)
        .should('exist')
        .and('be.visible')
    })
  }

  static selectCurrentDevice () {
    cy.enter().then(getBody => {
      getBody()
        .find('button')
        .contains(CURRENT_DEVICE_FIELD_TEXT)
        .click()
        .then(() => {
          return new StartSessionPage()
        })
    })
  }
}

export default DeviceSelectionPage
