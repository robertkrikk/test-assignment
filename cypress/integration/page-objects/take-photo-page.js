const TAKE_PHOTO_BUTTON = '[data-testid="take-photo"]'
const NO_PASSPORT_DETECTED_HEADING = '[data-test-inflow-title="true"]'
const NO_PASSPORT_DETECTED_TEXT = 'No passport detected'

class TakePhotoPage {
  static pressTakePhoto () {
    cy.enter().then(getBody => {
      getBody()
        .find(TAKE_PHOTO_BUTTON)
        .should('exist')
        .and('be.visible')
        .click()
    })
  }

  static passportValidationShouldFail () {
    cy.enter().then(getBody => {
      getBody()
        .find(NO_PASSPORT_DETECTED_HEADING)
        .contains(NO_PASSPORT_DETECTED_TEXT)
        .should('exist')
        .and('be.visible')
    })
  }
}

export default TakePhotoPage
