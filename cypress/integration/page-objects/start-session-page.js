import PassportReadyPage from './passport-ready-page'

const START_SESSION_TEXT = 'Start session'

class StartSessionPage {
  static pressStartSessionButton () {
    // Enter the iframe and check if user is in the new modal
    cy.enter().then(getBody => {
      getBody()
        .find('button')
        .contains(START_SESSION_TEXT)
        .should('exist')
        .and('be.visible')
        .click()
        .then(() => {
          return new PassportReadyPage()
        })
    })
  }
}

export default StartSessionPage
