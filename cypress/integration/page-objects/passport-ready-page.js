import TakePhotoPage from './take-photo-page'

const CONTINUE_BUTTON_TEXT = 'Continue'

class PassportReadyPage {
  static shouldBeVisible () {
    cy.enter().then(getBody => {
      getBody()
        .find('h1')
        .contains('Please have your passport ready.')
        .should('exist')
        .and('be.visible')
    })
  }

  static pressContinueButton () {
    // Enter the iframe and check if user is in the new modal
    cy.enter().then(getBody => {
      getBody()
        .find('button')
        .contains(CONTINUE_BUTTON_TEXT)
        .should('exist')
        .and('be.visible')
        .click()
        .then(() => {
          return new TakePhotoPage()
        })
    })
  }
}

export default PassportReadyPage
