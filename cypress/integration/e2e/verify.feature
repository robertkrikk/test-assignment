Feature: User should not be able to verify their identity if invalid passport is presented

  Scenario:
    Given User is on the home page
    When User fills the fields with valid values
    And Clicks Veriff Me
    And Clicks "Continue with current device"
    And Starts Verification session
    And Takes Photo Of Invalid Passport
    Then Document Is Not Recognized