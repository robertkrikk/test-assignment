import HomePage from '../../page-objects/home-page'
import { Given, When, Then, And } from 'cypress-cucumber-preprocessor/steps'
import DeviceSelectionPage from '../../page-objects/device-selection-page'
import StartSessionPage from '../../page-objects/start-session-page'
import PassportReadyPage from '../../page-objects/passport-ready-page'
import TakePhotoPage from '../../page-objects/take-photo-page'

const testData = require('../../../fixtures/test-data.json')

Given('User is on the home page', () => {
  HomePage.visit()
})

When('User fills the fields with valid values', () => {
  HomePage.enterFullName(testData.name)
  HomePage.selectCountry(testData.country)
  HomePage.selectLanguage(testData.language)
  HomePage.selectDocumentType(testData.documentType)
})

And('Clicks Veriff Me', () => {
  HomePage.pressVeriffMe()
})

And('Clicks "Continue with current device"', () => {
  DeviceSelectionPage.shouldBeVisible()
  DeviceSelectionPage.selectCurrentDevice()
})

And('Starts Verification session', () => {
  StartSessionPage.pressStartSessionButton()
  PassportReadyPage.shouldBeVisible()
  PassportReadyPage.pressContinueButton()
})

And('Takes Photo Of Invalid Passport', () => {
  TakePhotoPage.pressTakePhoto()
})

Then('Document Is Not Recognized', () => {
  TakePhotoPage.passportValidationShouldFail()
})
