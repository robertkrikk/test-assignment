/* eslint-disable no-unused-expressions */
import { CREATE_SESSION_URL, createRequestWithBody } from '../../api-helpers/api-helpers'

describe('/sessions endpoint tests', () => {
  it('Session should be created if payload is correct', () => {
    const requestBody = {
      full_name: 'Amara Maressa'
    }
    createRequestWithBody('POST', CREATE_SESSION_URL, undefined, requestBody).then(resp => {
      expect(resp.status).to.equal(200)
      expect(resp.body.integrationUrl).to.not.be.empty
      expect(resp.body.sessionToken).to.not.be.empty
    })
  })

  it('Session should not be created if first_name property is empty', () => {
    const requestBody = {
      full_name: ''
    }
    createRequestWithBody('POST', CREATE_SESSION_URL, undefined, requestBody).then(resp => {
      expect(resp.status).to.not.equal(200)
      expect(resp.body.integrationUrl).to.not.be.empty
      expect(resp.body.sessionToken).to.not.be.empty
    })
  })

  it('Session should not be created if payload is empty', () => {
    createRequestWithBody('POST', CREATE_SESSION_URL, undefined, undefined).then(resp => {
      expect(resp.status).to.not.equal(200)
      expect(resp.body.integrationUrl).to.not.exist
      expect(resp.body.sessionToken).to.not.exist
    })
  })
})
