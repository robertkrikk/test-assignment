/* eslint-disable no-unused-expressions */
import { CREATE_SESSION_URL, createRequestWithBody, SESSIONS_URL } from '../../api-helpers/api-helpers'

describe('/session tests', () => {
  before(() => {
    const requestBody = {
      full_name: 'Test Test'
    }
    createRequestWithBody('POST', CREATE_SESSION_URL, undefined, requestBody).then(resp => {
      cy.setLocalStorage('token', resp.body.sessionToken)
      cy.saveLocalStorage()
    })
  })

  beforeEach(() => {
    cy.restoreLocalStorage()
  })

  it('Should not be able to get session data without authentication', () => {
    createRequestWithBody('GET', SESSIONS_URL, undefined, {})
      .then(resp => {
        expect(resp.status).to.eq(401)
      })
  })

  it('Should be able to get session data with correct token', () => {
    cy.getLocalStorage('token').then(token => {
      const authorizationHeader = {
        authorization: `Bearer ${token}`
      }
      createRequestWithBody('GET', SESSIONS_URL, authorizationHeader, {})
        .then(resp => {
          expect(resp.status).to.eq(200)
          expect(resp.body.status).to.equal('created')
          expect(resp.body).to.include.all.keys(
            'activeProofOfAddressSession',
            'activeVerificationSession',
            'copyStrings',
            'initData',
            'id',
            'status',
            'previousProofOfAddressSessions',
            'previousVerificationSessions',
            'vendorIntegration')
        })
    })
  })
})
