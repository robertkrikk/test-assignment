# Test-assignment

## Summary
This repository contains automated UI tests & API tests for Veriff Test Task. BDD approach with POM design was used for the UI tests.
* UI Tests (.feature test files) are in `cypress/integration/e2e` folder
* API Tests are in `cypress/integration/api` folder

## Running with docker

* Authenticate with docker
* Pull cypress/included 9.6.0 using the next command `docker pull cypress/included:9.6.0`
* For running e2e tests with docker use the next command `npm run e2e-tests:chrome`
* For running api tests with docker use the next command `npm run api-tests`

## Test Results

* By default test results are saved into **/results** folder
  * Xml file is generated which can be used for uploading results in Test Management Tools like Xray etc
* For a visual mochaawesome report the user has to add  `--reporter mochawesome` to the end of run commands
  * A new folder mochaawesome-report will be generated to the root folder

## Running from CI
An example CI file is created in .gitlab-ci.yml. The main goal of this file is to execute the
tests when a pipeline is triggered by TEST_TYPE variable. Currently the pipeline fails as
there is no runner set up in CI/CD, but the flow would look next:
* Change in FE/API repository will trigger a downstream pipeline including this repository
* Variable TEST_TYPE is sent to the current repository and will have one of the next values:
  * If pipeline was triggered from FE repository, the TEST_TYPE variable will have value "e2e"
  * If pipeline was triggered from API repository, the TEST_TYPE variable will have value "api"
* Correct tests are executed based on the variable and results will be shown in the GitLab CI Terminal
